Rhatto's dotfiles bundle
========================

This is the bundle repository for rhatto's dotfiles.
More information at https://git.fluxo.info/metadot

Dependencies
------------

Each module might have it's own dependency listing at the `dependencies/` folder.
You might also want to bundle this dotfiles repository with https://git.fluxo.info/rhatto/apps
